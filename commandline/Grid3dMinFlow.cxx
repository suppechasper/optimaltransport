#include "Precision.h"

#include <stdio.h>

#include "Grid3dMinFlow.h"
#include <Eigen/Dense>
#include "EigenLinalgIO.h"

#include <tclap/CmdLine.h>

#include "LemonSolver.h"

#include "Random.h"

int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Matrix Minimum FLow", ' ', "1");


  try{
	  cmd.parse( argc, argv );
	}
  catch (TCLAP::ArgException &e){
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return -1;
  }


  Grid3dMinFlow<Precision>::TensorXp S(50, 50, 50);
  Grid3dMinFlow<Precision>::TensorXp T(50, 50, 50);
  Random<Precision> random;
  for(int i=0; i<50; i++){
  for(int j=0; j<50; j++){
  for(int k=0; k<50; k++){
     S(i, j, k) = random.Uniform();
     T(i, j, k) = random.Uniform();
  }}}

  Grid3dMinFlow<Precision> mmf;

  LemonSolver lpSolver;
  std::map< std::pair<long, long>, Precision> plan = mmf.solve(S, T, lpSolver);


  return 0;

}
